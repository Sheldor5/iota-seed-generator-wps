@ECHO OFF
PowerShell -ExecutionPolicy Bypass -Command "&{"^
 "class RandomSeed {"^
 "  hidden [System.Security.Cryptography.RNGCryptoServiceProvider] $RNG = [System.Security.Cryptography.RNGCryptoServiceProvider]::new();"^
 "  hidden [String] $Alphabet;"^
 "  RandomSeed ([String] $alphabet) {"^
 "    $this.Alphabet = $alphabet;"^
 "  }"^
 "  [String] NewSeed([int] $length) {"^
 "    $seed = '';"^
 "    $rand = New-Object byte[] 1;"^
 "    for ($i = 0; $i -lt $length; $i++) {"^
 "      do {"^
 "        $this.RNG.GetBytes($rand);"^
 "      } while ($rand[0] -lt 0 -or $rand[0] -ge $this.Alphabet.Length);"^
 "      $seed += $this.Alphabet.Substring($rand[0], 1);"^
 "    }"^
 "    return $seed;"^
 "  }"^
 "}"^
 "class IotaSeed {"^
 "  hidden static [String] $ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ9';"^
 "  hidden static [int] $LENGTH = 81;"^
 "  hidden [RandomSeed] $RNG = [RandomSeed]::new([IotaSeed]::ALPHABET);"^
 "  [String] NewSeed() {"^
 "	  return $this.RNG.NewSeed([IotaSeed]::LENGTH);"^
 "  }"^
 "}"^
 "[IotaSeed] $isg = [IotaSeed]::new();"^
 "[String] $seed = $isg.NewSeed();"^
 "Set-Clipboard -Value $seed;"^
 "Write-Host 'Your new seed is: ' -nonewline;"^
 "Write-Host $seed -foregroundcolor 'Cyan';"^
 "Write-Host 'Your seed was copied to your clipboard!';"^
 "Write-Host '';"^
 "Write-Host 'Press any key to exit ...';"^
 "$tmp = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');"^
 "}"
